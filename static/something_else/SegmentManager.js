class SegmentManager {
    constructor() {
        this.segments = [];
    }

    GetSegmentFromPosition(position) {
        for (let segment of this.segments) {
            if (position.x > segment.topLeft.x &&
                position.y > segment.topLeft.y &&
                position.x < segment.bottomRight.x &&
                position.y < segment.bottomRight.y) {
                return segment;
            }
        }
        return null;
    }
}