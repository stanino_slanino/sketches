let imageWidth;
let imageHeight;
let pointSize = 4;
let segmentCount = 10;
let pointCount;
let allPoints = [];
let image_object;
let segmentManager;

function  preload() {
    image_object = JSON.parse(image_data);
}

function setup() {
    imageWidth = image_object.size[0];
    imageHeight = image_object.size[1];
    pointCount = Math.floor((image_object.size[0]*image_object.size[1])/pointSize);
    createCanvas(imageWidth, imageHeight);
    segmentManager = new SegmentManager();
    // frameRate(14);
    let segmentWidth = canvas.width/segmentCount;
    let segmentHeight = canvas.height/segmentCount;
    for (let i = 0; i < segmentCount; i++) {
        for (let j = 0; j < segmentCount; j++) {
            let segment = new Segment({x:segmentWidth*j, y:segmentHeight*i},{x:segmentWidth+segmentWidth*j, y:segmentHeight+segmentHeight*i});
            segmentManager.segments.push(segment);
        }
    }

    for (let i=0; i<image_object.pixels.length; i++) {
        let row = image_object.pixels[i];
        for (let j=0; j<row.length; j++) {
            let new_point = new Point(i*4, j*4, row[j])
            let segment = segmentManager.GetSegmentFromPosition(new_point.position);
            if (segment) {
                segment.points.push(new_point);
            }
            allPoints.push(new_point);
        }
    }
    noStroke();
}

function draw() {
    // background(255);
    if (frameCount === 1) {
        for (let drawPoint of allPoints) {
            drawPoint.Draw();
        }
    }
    let mousePosition = {x:mouseX, y:mouseY};
    let activeSegment = segmentManager.GetSegmentFromPosition(mousePosition);
    if (!activeSegment) {
        return;
    }

    for (let movePoint of activeSegment.points) {
        movePoint.ShiftPosition();
        movePoint.Draw();
    }

}