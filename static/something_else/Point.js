class Point {
    constructor(x,y,color) {
        this.position = ({x:x,y:y});
        this.color=color;
    }

    Draw() {
        fill(this.color);
        ellipse(this.position.x, this.position.y, pointSize);
    }

    ShiftPosition() {
        this.position.x += random(-2,2);
        this.position.y += random(-2,2);
    }
}