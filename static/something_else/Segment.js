class Segment {
    constructor(x,y) {
        this.points = [];
        this.topLeft = x;
        this.bottomRight = y;
    }
}