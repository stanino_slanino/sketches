let centerPoint;



function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  centerPoint = createVector(windowWidth/2, windowHeight/2);
}



//p5.js function that draws the image
function draw() {
    fill(255,0,255);
    ellipse(centerPoint.x, centerPoint.y, 5);
    stroke(255);
    let mousePosition = createVector(mouseX, mouseY);
    let mirrorPoint = p5.Vector.sub(centerPoint, mousePosition);
    point(mousePosition.x, mousePosition.y);
    translate(mousePosition, centerPoint.y);
    point(mousePosition.x, mirrorPoint.y);
    translate(centerPoint.x, centerPoint.y);
    point(mirrorPoint.x, mirrorPoint.y);

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
  background(0);;
}
