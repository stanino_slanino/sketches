/*
This is the main function. I was unsure whether I should put the event that calls the function here, so I also
created the shootGun function to simulate the event. It uses an iteration cycle, which is not terrible effective,
but it does the job. All other funtions (and fake objects) I used for testing are located in a separate file called
dummy_functions.js.

The main function (called getEffect) takes soldier and gun objects as its input parameters, both pushed from game
environment. The function uses local coordinates relative to the muzzle position both for gun's direction and for
soldier position.

Then, it computes the distances between muzzle and soldier. For computing the angle, I chose to use 2D directional
vectors, to simplify the code and keep consistent with the sketch in the assignment. I am still checking for the
distance in 3D, so if the soldier is much higher than the tank, he should not be affected.

If the distance is greater than the high effect diameter, it skips the soldier. If it is closer, it tests whether:

1. The soldier is between the min and max vectors of highEffect, if so, it checks if it is inside the flashEffect
radius. if both conditions are true, it applies full effect lineary scaled to the distance from the muzzle. Although
sound pressure and light intensity have a inverse quadratic function of 1/d*d, I think that the subjective effects
themselves should be linear. If needed, it could easily be adjusted.

2. The soldier is between the min and max vectors of highEffect, if so, it checks if it is inside the flashEffect
radius. if both conditions are true, it applies half of the effect lineary scaled to the distance from the muzzle.

3. Soldier is between the min and max vectors of lowEffectA, if so, it checks if it is inside the lowEffectrange
radius. If both conditions are true, it applies deafness only, but as the muzzle brake forces most of the sound
to the sides, I did not halve the deafness duration.

4. Soldier is between the min and max vectors of lowEffectB, if so, it checks if it is inside the lowEffectrange
radius. If both conditions are true, it applies deafness only in the same way as previous case.

If a more general isInArc(posistion, minAngle, maxAngle, radius) function with Boolean return would be avalailable,
I would have used it instead of individual checks.

The if..else if..else cluster is not very elegant, but it does the job. If there is a more efficient way of doing it,
I am yet to learn it.
*/

let textBox = [];


function getEffect(soldier, gun) {
    //calculates distance between muzzle and soldier
    var distance = p5.Vector.dist(soldier.pos, gun.pos);
    //excludes all soldiers outside the greatest of ranges
    if (distance <= gun.highEffectRange)
    {

        soldier.pos2d = new createVector(soldier.pos.x, soldier.pos.y);
        var angle = p5.Vector.angleBetween(soldier.pos2d, gun.gunDir2d);

        //soldier is in HEA and WITHIN the FEA
        if (angle <= gun.highEffectArcmin && angle >= gun.highEffectArcmax && distance <= gun.flashEffectRange) {
            blindenessDuration = 1 + ((maxBlindenessDuration * (1 - (distance / gun.highEffectRange))) / 2);
            deafnessDuration = 30 + ((maxDeafnessDuration * (1 - (distance / gun.highEffectRange))) / 2);
            setDeafness(soldier, deafnessDuration);
            setBlindeness(soldier, blindenessDuration);
            return 2 //for visualisation only
        }

        //soldier is in HEA and OUTSIDE the FEA
        else if (angle <= gun.highEffectArcmin && angle >= gun.highEffectArcmax && distance > gun.flashEffectRange) {
            blindenessDuration = maxBlindenessDuration * 0.5 * (1 - (distance / gun.highEffectRange));
            deafnessDuration = maxDeafnessDuration * 0.5 * (1 - (distance / gun.highEffectRange));
            setDeafness(soldier, deafnessDuration);
            setBlindeness(soldier, blindenessDuration);
            return 1 //for visualisation only
        }

        //soldier is within LEA to the RIGHT of the gun
        else if (angle >= gun.lowEffectArcAmin && angle <= gun.lowEffectArcAmax && distance <= gun.lowEffectRange) {
            deafnessDuration = maxDeafnessDuration * (1 - (distance / gun.lowEffectRange));
            setDeafness(soldier, deafnessDuration);
            return 3 //for visualisation only
        }

        //soldier is within LEA to the LEFT of the gun
        else if (angle >= gun.lowEffectArcBmin && angle <= gun.lowEffectArcBmax && distance <= gun.lowEffectRange) {
            deafnessDuration = maxDeafnessDuration * (1 - (distance / gun.lowEffectRange));
            setDeafness(soldier, deafnessDuration);
            return 3 //for visualisation only
        }

        else { return 0; }
    } else { return 0; }
}


//simulates event that fires the gun and checks for collateral damage
function shootGun(gun) {
    for (i = 0; i < allUnits.length; i++)
    {
        var soldier = allUnits[i];
        soldier.effect = getEffect(soldier, gun);
    }
}
