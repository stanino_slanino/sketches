/// <reference path="libraries/p5.js" />
/// <reference path="main_function.js" />
/// <reference path="sketch.js" />

/*
In this section, I store most of the important properties of soldiers and the gun. The variables that have
been given in the assignemnt are on the very top, I also aded angles of the effect arcs. I assumed that
100 degrees and 80 degrees for high and low effects respectively should approximately copy the real effects.

I left the specific min and max vectors (the boundaries of the arcs) as a parameters of the gun.

Also, soldier coordinates are given as local coordinates relative to the muzzle position. If they were originally
given as global coordinates, they would be translated to local coordinates by a rather simple function.
*/



var allUnits = [];
var muzzleCaliber = 120; //hardcoded, mm, should be pushed from game
var muzzleDir = [-1, 0, 0]; //hardcoded, unit vector, should be pushed from game
var muzzlePos = [0, 0, 0]; //hardcoded, should be pushed from game
var maxDeafnessDuration = 60;
var maxBlindenessDuration = 2;
var highEffectAngle = toRadians(100);
var lowEffectAngle = toRadians(80);


//creates a gun object for testing
function createGun(cal, gunPos, gunDir) {
    this.caliber = cal;
    this.pos = createVector(gunPos[0], gunPos[1], gunPos[2]); //creates p5.js vector
    this.dir = createVector(gunDir[0], gunDir[1], gunDir[2]); //creates p5.js vector
    this.highEffectRange = cal * 2.5;
    this.flashEffectRange = cal * 0.75;
    this.lowEffectRange = cal * 1.25;
    this.highEffectArcmin = highEffectAngle / 2;
    this.highEffectArcmax = -1 * highEffectAngle / 2;
    this.lowEffectArcAmin = Math.PI / 2 - lowEffectAngle / 2;
    this.lowEffectArcAmax = Math.PI / 2 + lowEffectAngle / 2;
    this.lowEffectArcBmin = (-1 * Math.PI / 2) - lowEffectAngle / 2;
    this.lowEffectArcBmax = (-1 * Math.PI / 2) + lowEffectAngle / 2;
    this.gunDir2d = createVector(this.dir.x, this.dir.y).normalize();
    // this.gunDir2d.normalize; //creates 2d directional vector to simplify math
}

//defines a soldier object for testing
function Soldier(id) {

    //limits for spawning soldiers
    var minposx = -400;
    var maxposx = 400;
    var minposy = -300;
    var maxposy = 300;
    //soldier is given random coordinates
    var posx = Math.random() * (maxposx - minposx) + minposx;
    var posy = Math.random() * (maxposy - minposy) + minposy;
    //I used 0 as z position to simplify
    var posz = 0;


    this.name = "Soldier " + id;
    this.pos = new createVector(posx, posy, posz);
    this.effect = 0;
}

//populates gun's surroundings by soldiers
function createSoldiers() {
    for (i = 0; i < 20; i++) {
        newSold = new Soldier(i);
        allUnits.push(newSold);
    }
}

//dummy implementation of setBlindness
function setBlindeness(soldier, duration) {
    let round_duration = Math.round(duration * 100) / 100;
    let new_status = (soldier.name + ' was blinded for ' + round_duration + ' seconds.');
    textBox.push(new_status);
}

//dummy implementation of setDeafness
function setDeafness(soldier, duration) {
    let round_duration = Math.round(duration * 100) / 100;
    let new_status = (soldier.name + ' was deafened for ' + round_duration + ' seconds.');
    textBox.push(new_status);
}

//returns angle in radians
function toRadians(angle) {
    return angle * (Math.PI / 180);
}

//returns local coordinates of soldier relative to muzzle
function localCoordinates(posPoint, posOrigin) {
    return posPoint.pos;
}
