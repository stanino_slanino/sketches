//p5.js function that is executed on init
function setup() {
    createCanvas(1300, 600);
    background(255);
    createSoldiers();
    bigGun = new createGun(muzzleCaliber, muzzlePos, muzzleDir);
    shootGun(bigGun);
    }


//p5.js function that draws the image
function draw() {
    noLoop();
    translate(400, 300);
    var zeroVector = createVector(-1, 0, 0);
    var dirCorrection = p5.Vector.angleBetween(zeroVector, bigGun.gunDir2d);
    fill('rgba(100%,0%,100%,1)');
    arc(bigGun.pos.x, bigGun.pos.y, 2*bigGun.highEffectRange, 2*bigGun.highEffectRange, bigGun.highEffectArcmax - PI - dirCorrection, bigGun.highEffectArcmin - PI - dirCorrection);
    fill('rgba(0%,0%,100%,0.5)');
    arc(bigGun.pos.x, bigGun.pos.y, 2*bigGun.flashEffectRange, 2*bigGun.flashEffectRange, bigGun.highEffectArcmax + PI - dirCorrection, bigGun.highEffectArcmin + PI - dirCorrection);
    fill('rgba(100%,0%,0%,0.5)');
    arc(bigGun.pos.x, bigGun.pos.y, 2*bigGun.lowEffectRange, 2*bigGun.lowEffectRange, bigGun.lowEffectArcBmin - dirCorrection, bigGun.lowEffectArcBmax - dirCorrection);
    fill('rgba(100%,0%,0%,0.5)');
    arc(bigGun.pos.x, bigGun.pos.y, 2*bigGun.lowEffectRange, 2*bigGun.lowEffectRange, bigGun.lowEffectArcAmin - dirCorrection, bigGun.lowEffectArcAmax - dirCorrection);
    fill(255, 255, 255, 100);
    for (i = 0; i < allUnits.length; i++) {
        soldier = allUnits[i];
        //chooses fill color for ellipse
        switch (soldier.effect) {
            case 1:
                fill(0, 0, 255);
                break;
            case 2:
                fill(0, 255, 0);
                break;
            case 3:
                fill(0, 0, 0);
                break;
            default:
                fill(255);
        }
        //draws a circle of chosen color to represent soldier
        ellipse(soldier.pos.x, soldier.pos.y, 7, 7);
        fill(0);
        text(soldier.name, soldier.pos.x, soldier.pos.y - 10);
    }
    fill(0);
    stroke(0);
    ellipse(0,0,30);
    text("BIG GUN", 0, 30);

    let statusPos = -280;
    for (status of textBox) {
        text(status, 600, statusPos)
        statusPos += 20;
    }

}
