Contents of this package:
libraries/ - contain the p5.js library I used for visualization and vector math
functions.
main_function.js - the actual function + dummy function that fires the gun and
tests the position of soldiers
dummy_functions.js - contains all functions and objects I would expect to either
 exist in the game environment, or be pre-programmed.
