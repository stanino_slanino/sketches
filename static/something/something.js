let things = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  for (let i=0; i<20; i++) {
      let new_thing = new Thing(Math.random()*windowWidth, Math.random()*windowHeight, Math.floor(random(255)));
      things.push(new_thing);
  }
  things.sort((a,b) => b.size-a.size);
  background(255);
}

function draw() {
    background(255);
    noStroke();
    if (things.length<0) {
        return;
    }
    for (let i = 0; i<things.length; i++) {
        let thing = things[i];
        thing.Draw();
    }
    things.sort((a,b) => b.size-a.size);
}