class Thing {

    constructor(x,  y, starting_color) {
        this.x = x;
        this.y = y;
        console.log(starting_color);
        this.size = starting_color +20;
        let clr = starting_color;
        this.color = [clr, clr, clr]
    }

    GetNewPosition() {
        this.x = Math.random()*windowWidth;
        this.y = Math.random()*windowHeight;
    }

    GetNewColor() {
        if (this.color[2] <= 255) {
            let clr = this.color[0];
            clr++;
            this.size++;
            this.color = [clr, clr, clr];
        }
        else {
            this.color = [0, 0, 0];
            this.size = 20;
            this.y = 0;
            this.GetNewPosition();
        }
    }

    Draw(){
        this.GetNewColor();
        fill(this.color);
        ellipse(this.x, this.y, this.size);
    }
}