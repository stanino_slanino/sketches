from flask import Flask, render_template
from PIL import Image
from json import load

app = Flask(__name__, static_url_path='/static')

@app.after_request
def add_header(r):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    r.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    r.headers["Pragma"] = "no-cache"
    r.headers["Expires"] = "0"
    r.headers['Cache-Control'] = 'public, max-age=0'
    return r

@app.route('/concussion')
def concussion():
    return render_template('concussion.html')


@app.route('/flowers')
def flowers():
    return render_template('flowers.html')


@app.route('/lightning')
def lightning():
    return render_template('lightning.html')


@app.route('/white')
def white():
    return render_template('white.html')


@app.route('/something')
def something():
    return render_template('something.html')


@app.route('/something_else')
def something_else():
    im = Image.open('static/something_else/foto_2.png')
    px = im.load()
    image = {}
    pixels = []
    size = im.size
    for i in range(0, im.size[0]):
        if i%4 != 0:
            continue
        row = []
        for j in range(0, im.size[1]):
            if j % 4 != 0:
                continue
            triplet = [px[i,j][0],px[i,j][1],px[i,j][2]]
            row.append(triplet)

        pixels.append(row)
    image["size"] = size
    image["pixels"] = pixels
    print(image)
    return render_template('something_else.html', image_data=image)


@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()
